/* BitArray.h
 *
 * Copyright (C) 2014 MongoDB, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef BIT_ARRAY_H
#define BIT_ARRAY_H


#include <Debug.h>
#include <Macros.h>
#include <Types.h>


BEGIN_DECLS


/*
 *--------------------------------------------------------------------------
 *
 * BitArray_Set --
 *
 *       Set a given bit in the bit array.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static inline void
BitArray_Set (void *buf,  /* IN */
              size_t bit, /* IN */
              bool value) /* IN */
{
   if (value) {
      ((uint8_t *)buf) [bit >> 3] |= (1U << (bit & 0x7));
   } else {
      ((uint8_t *)buf) [bit >> 3] &= ~(1U << (bit & 0x7));
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * BitArray_Get --
 *
 *       Fetch a given bit in the bit array.
 *
 * Returns:
 *       0 or 1, depending on the bit status.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static inline bool
BitArray_Get (void *buf,  /* IN */
              size_t bit) /* IN */
{
   return ((((uint8_t *)buf) [bit >> 3] >> (bit & 0x7)) & 0x1);
}


END_DECLS


#endif /* BIT_ARRAY_H */
